﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BasicEnemy : MonoBehaviour
{
    [SerializeField]
    int health;
    [SerializeField]
    TextMeshProUGUI healthText;

    private void Update()
    {
        healthText.text = "HP: " + health;
    }
    public int Health
    {
        get { return health; }
        set { health = value; }
    }
}
