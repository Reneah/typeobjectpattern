﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Weapon")]
public class Sword : ScriptableObject
{
    [SerializeField]
    private int attackDamage;
    [SerializeField]
    private int durability;
    [SerializeField]
    Sprite mySprite;

    public int AttackDamage
    {
        get { return attackDamage; }
        set { attackDamage = value; }
    }

    public int Durability
    {
        get { return durability; }
        set { durability = value; }
    }

    public Sprite MySprite
    {
        get { return mySprite; }
        set { mySprite = value; }
    }

}
