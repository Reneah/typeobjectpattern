﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Weapon : MonoBehaviour
{
    [SerializeField]
    public Sword currentSword;

    [SerializeField]
    BasicEnemy target;
    private int durabilityLoss = 10;
    [SerializeField]
    TextMeshProUGUI durabilityText;


    private void Update()
    {
        durabilityText.text = "Durability: " + currentSword.Durability;
        GetComponent<SpriteRenderer>().sprite = currentSword.MySprite;
        Debug.Log("Current Weapon is " + currentSword);
    }

    public void Attack()
    {
        target.Health -= currentSword.AttackDamage;
        currentSword.Durability -= durabilityLoss;
    }
}
